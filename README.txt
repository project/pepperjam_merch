$Id

-- SUMMARY --

The pepperjam_merch module implements the Pixel Integration needed for feeding 
itemized sales from an Ubercart instance to the PepperJam Network.

It also manages the Item Lists that are required by PepperJam in order to 
manage your network.

-- REQUIREMENTS --

1. A Merchant Partner Account with PepperJam
2. Ubercart

-- INSTALLATION --

1. install the module.  sites/all/modules
2. enable admin->site-building->moduldes->other

-- CONFIGURATION --

1. use admin to set PepperJam Merchant ID
2. use admin to map products to PJN List ID's you generated in the PJN Manage Partners Iface.
3. move the page-pjn_merch.tpl.php to your themes area.

-- CONTACT --

Current maintainers:
* Stephen G. Wills (Blessed_Steve) - swills@beyond-print.com


-- TODO --
to do:
test eclipse environment for editing project
Admin interface for List ID creation.
